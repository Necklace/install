ServerRoot "/etc/httpd"

Listen 80

LoadModule access_compat_module modules/mod_access_compat.so
LoadModule alias_module modules/mod_alias.so
LoadModule auth_basic_module modules/mod_auth_basic.so
LoadModule authn_core_module modules/mod_authn_core.so
LoadModule authn_file_module modules/mod_authn_file.so
LoadModule authz_core_module modules/mod_authz_core.so
LoadModule authz_groupfile_module modules/mod_authz_groupfile.so
LoadModule authz_host_module modules/mod_authz_host.so
LoadModule authz_user_module modules/mod_authz_user.so
LoadModule autoindex_module modules/mod_autoindex.so
LoadModule dir_module modules/mod_dir.so
LoadModule env_module modules/mod_env.so
LoadModule filter_module modules/mod_filter.so
LoadModule headers_module modules/mod_headers.so
LoadModule include_module modules/mod_include.so
LoadModule log_config_module modules/mod_log_config.so
LoadModule mime_module modules/mod_mime.so
LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
LoadModule negotiation_module modules/mod_negotiation.so
LoadModule php7_module modules/libphp7.so
LoadModule reqtimeout_module modules/mod_reqtimeout.so
LoadModule rewrite_module modules/mod_rewrite.so
LoadModule setenvif_module modules/mod_setenvif.so
LoadModule slotmem_shm_module modules/mod_slotmem_shm.so
LoadModule status_module modules/mod_status.so
LoadModule unixd_module modules/mod_unixd.so
LoadModule userdir_module modules/mod_userdir.so
LoadModule version_module modules/mod_version.so

AddHandler php7-script .php

<IfModule unixd_module>
	User http
	Group http
</IfModule>

ServerAdmin ns@nsz.no

ServerName localhost

<Directory />
	AllowOverride none
	Require all denied
</Directory>

DocumentRoot "/srv/http"

<Directory "/srv/http">

	SetEnv MAKO_ENV dev

	SetEnv AD_OAUTH_CLIENT_ID 3
	SetEnv AD_OAUTH_CLIENT_SECRET 1f28555f-f245-492c-b231-e1f75fe06620
	SetEnv AD_OAUTH_REDIRECT_URI http://localhost:8000/archive-digitisation/public/index.php/auth/oauth

	Options Indexes FollowSymLinks

	AllowOverride All

	Require all granted
</Directory>

<IfModule dir_module>
	DirectoryIndex index.html
</IfModule>

<Files ".ht*">
	Require all denied
</Files>

ErrorLog "/var/log/httpd/error_log"

LogLevel warn

<IfModule log_config_module>

	LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
	LogFormat "%h %l %u %t \"%r\" %>s %b" common

	<IfModule logio_module>
	  LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
	</IfModule>

	CustomLog "/var/log/httpd/access_log" common

</IfModule>

<IfModule headers_module>
	RequestHeader unset Proxy early
</IfModule>

<IfModule mime_module>
	TypesConfig conf/mime.types
	AddType application/x-compress .Z
	AddType application/x-gzip .gz .tgz
</IfModule>

Include conf/extra/httpd-mpm.conf
Include conf/extra/httpd-multilang-errordoc.conf
Include conf/extra/httpd-autoindex.conf
Include conf/extra/httpd-languages.conf
Include conf/extra/httpd-userdir.conf
Include conf/extra/httpd-default.conf

<IfModule proxy_html_module>
	Include conf/extra/proxy-html.conf
</IfModule>

Include conf/extra/php7_module.conf

<IfModule ssl_module>
	SSLRandomSeed startup builtin
	SSLRandomSeed connect builtin
</IfModule>
