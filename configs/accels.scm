; Unset shift+arrow/page keys for Micro
(gtk_accel_path "<Actions>/terminal-widget/shift-up" "")
(gtk_accel_path "<Actions>/terminal-widget/shift-down" "")
(gtk_accel_path "<Actions>/terminal-widget/shift-pageup" "")
(gtk_accel_path "<Actions>/terminal-widget/shift-pagedown" "")
