#!/usr/bin/env python3
# -*- coding: utf8 -*-


import sys
import shutil
import pathlib



if __name__ == '__main__':

    if not 'linux' in sys.platform:
        print('Not running on GNU/Linux. Quitting update.')
        sys.exit()

    current_folder = pathlib.Path(__file__).parent

    repo_folder = current_folder / 'configs/xfce4'

    configs_folder = pathlib.Path.home() / '.config/xfce4/xfconf/xfce-perchannel-xml/'

    for repo_file in repo_folder.glob('*.xml'):
        shutil.copyfile(configs_folder / repo_file.name, repo_file)





