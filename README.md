
# Necklace / Install

This is my personal collection of installation scripts (btw, I use Arch GNU/Linux).

## How to install

*WARNING THIS WILL DELETE ALL DATA ON THE SPECIFIED INSTALL DISK*

1. Boot up Arch GNU/Linux live ISO
2. `loadkeys {layout}` (I use `no`, configs here are all set to use no + winkeys)
3. `wget bitbucket.com/Necklace/install/get/HEAD.tar.gz` / `curl bitbucket.com/Necklace/install/get/HEAD.tar.gz -o HEAD.tar.gz`
4. `tar -xzf HEAD.tar.gz`
5. `cd Necklace-install-{REF}` (hit TAB after typing N)
6. `./install`
7. Follow install


## Attribution

- Inspired by [Van Nguyen / Shirotech](https://shirotech.com/linux/how-to-automate-arch-linux-installation)
- Many thanks to [Pavel Kogan for the FDE instructions](https://web.archive.org/web/20180103175714/http://www.pavelkogan.com/2014/05/23/luks-full-disk-encryption/)
- Many thanks to [Libreboot for the cryptsetup recommended ciphers and iter time](https://libreboot.org/docs/gnulinux/encrypted_parabola.html)
- Many thanks to [The Practical Linux Hardening Guide](https://github.com/trimstray/the-practical-linux-hardening-guide)
- https://ruderich.org/simon/notes/secure-boot-with-grub-and-signed-linux-and-initrd
- https://gist.github.com/huntrar/e42aee630bee3295b2c671d098c81268
