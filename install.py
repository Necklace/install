#!/usr/bin/env python3
# -*- coding: utf8 -*-


import os
import sys
import json
import random
import shutil
import signal
import socket
import string
import getpass
import pathlib
import argparse
import datetime
import fileinput
import contextlib
import subprocess
import configparser
import urllib.request


class run:

    @staticmethod
    def ignore_error(arguments=[], stdin='', stdout=subprocess.DEVNULL):
        subprocess.run(arguments, stdout=stdout, input=stdin.encode(), stderr=subprocess.DEVNULL)

    @staticmethod
    def return_bool(arguments=[]):
        return not bool(subprocess.run(arguments, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL).returncode)

    @staticmethod
    def warn_on_error(arguments=[], message='', stdin='', stdout=subprocess.DEVNULL):
        if subprocess.run(arguments, stdout=stdout, input=stdin.encode()).returncode:
            if not message:
                print('Error: %s failed' % arguments[0])
            else:
                print('Error: %s' % message)

    @staticmethod
    def raise_on_error(arguments=[], message='', stdin='', stdout=subprocess.DEVNULL):
        if subprocess.run(arguments, stdout=stdout, input=stdin.encode()).returncode:
            raise RuntimeError

    @staticmethod
    def quit_on_error(arguments=[], message='', stdin='', stdout=subprocess.DEVNULL):
        if subprocess.run(arguments, stdout=stdout, input=stdin.encode()).returncode:
            if not message:
                print('Error: %s failed' % arguments[0])
            else:
                print('Error: %s' % message)
            sys.exit()


# Enum instead?

install_types = {
    '1': 'Desktop install - XFCE desktop system (full disk encryption)',
    '2': 'Minimal install - barebones system (unencrypted)',
    '3': 'Minimal install - barebones system (full disk encryption)',
    '4': 'Virtual install - VM barebones system (unencrypted)'
}


def signal_handler(sig, frame):
    print('')
    print('Quitting installation.')
    sys.exit()


def get_disk(args):

    if args.disk:
        # Are more checks necessary? Check if actually in /dev/ ?
        if not pathlib.Path(args.disk).exists():
            print('Supplied disk does not exist.')
            args.disk = ''

    if not args.disk:

        print('Choose disk to install to:')

        disks = {}

        for i, disk in enumerate([x.stem for x in list(pathlib.Path('/sys/block').iterdir()) if 'pci' in str(x.resolve())]):
            print('%s: /dev/%s' % (i+1, disk))
            disks[str(i+1)] = disk

        option = ''
        while not option in disks.values():
            try:
                option = disks[input('Option: ')]
            except KeyError:
                print('Invalid input. Choose from the list above.')

        args.disk = '/dev/' + option


def determine_distro():

    if not 'linux' in sys.platform:
        print('Not running on GNU/Linux. Quitting install.')
        sys.exit()

    run.quit_on_error(['pacman', '--version'], 'Not running on an Arch-based distribution. Quitting install.')

    if not subprocess.run(['pacman', '-Qs', 'parabola-keyring'], stdout=subprocess.PIPE).returncode:
        return 'parabola'

    if not subprocess.run(['pacman', '-Qs', 'hyperbola-keyring'], stdout=subprocess.PIPE).returncode:
        return 'hyperbola'

    if not subprocess.run(['pacman', '-Qs', 'manjaro-keyring'], stdout=subprocess.PIPE).returncode:
        return 'manjaro'

    if not subprocess.run(['pacman', '-Qs', 'archlinux-keyring'], stdout=subprocess.PIPE).returncode:
        return 'archlinux'


def determine_gateway():

    ip = socket.gethostbyname(socket.gethostname()).split('.')

    if ip[-1] == '1':
        # Err, well, most likely not then? Ignore I suppose
        return ''

    return '.'.join(ip[:-1] + ['1:8000'])


def crypt(password):
    # Goodbye, crypt. Removed for no fucking reason by Python 3.13.

    return subprocess.run(['openssl', 'passwd', '-6', password], stdout=subprocess.PIPE).stdout.decode('utf8').strip()


def get_partition(args, n=1):

    partition = args.disk + str(n)

    if 'nvme' in args.disk:
        partition = args.disk + 'p' + str(n)

    return partition


def get_uuid(partition):

    return subprocess.run(['blkid', '-s', 'UUID', '-o', 'value', partition], stdout=subprocess.PIPE).stdout.decode('utf8').strip()


def list_installs():
    print('Possible install types:')
    for key, value in install_types.items():
        print('%s: %s' % (key, value))


def get_install(args):
    if not args.install:
        print('Choose install type.')
        list_installs()

        while args.install not in install_types.keys():
            args.install = input('Option: ')
            if args.install not in install_types.keys():
                print('Invalid option, try again.')
            if not args.install.isdigit():
                print('Needs to be a number, try again.')

    args.install = int(args.install)


def confirm_options(args):
    print('Options recap:')
    print('Install type: %s' % install_types[str(args.install)])
    print('Install disk: %s' % args.disk)
    print('Hostname: %s' % args.hostname)
    print('Username: %s' % args.username)
    print('GPU: %s' % args.gpu)

    if not args.noconfirm:
        continue_install = input('Does this look good? [Y/n]: ').lower()
        if continue_install != 'y' and continue_install != '':
            print('')
            print('Quitting installation.')
            sys.exit()

        print('Great! In the future you can use this command to install:')
        print('%s -i %s -d %s -n %s -u %s' % (sys.argv[0], args.install, args.disk, args.hostname, args.username))


def get_username(args):
    if not args.username:
        args.username = input('Set username: ')


def get_hostname(args):
    if not args.hostname:
        args.hostname = input('Set hostname: ')

    for illegal in string.whitespace:
        if illegal in args.hostname:
            print('Error: illegal whitespace character in hostname. Quitting.')
            sys.exit()


def get_passwords(args):

    print('Time to set passwords.')

    # Is a confirmation necessary? Default, but opt out with flag? Should passwords be cli arguments as well?

    args.encpass = ''

    if args.install == 1 or args.install == 3:
        args.encpass = getpass.getpass('Set encryption password: ')

    args.rootpass = getpass.getpass('Set password for root: ')

    args.userpass = getpass.getpass('Set password for %s: ' % args.username)


def determine_gpu():

    gpu = 'None'

    lspci = subprocess.run(['lspci'], stdout=subprocess.PIPE).stdout.decode('utf8')

    gpus = []

    for line in lspci.split('\n'):
        if 'VGA' in line: # or '3D' in line
            gpus.append(line)

    if len(gpus) == 1:
        if 'NVIDIA' in gpus[0]:
            gpu = 'NVIDIA'
        if 'AMD' in gpus[0]:
            gpu = 'AMD'
        if 'Intel' in gpus[0]:
            gpu = 'Intel'
    else:
        gpu = 'Hybrid'

    return gpu


def copy_to(frompath, topath, parents=True, exist_ok=True):

    # Make sure we're working with paths

    from_path = pathlib.Path(frompath)
    to_path = pathlib.Path(topath)

    # Make sure parent directories exist

    to_path.parent.mkdir(parents=parents, exist_ok=exist_ok)

    if from_path.is_file():

        shutil.copyfile(from_path, to_path)

    if from_path.is_dir():

        shutil.copytree(from_path, to_path)


def write_to(data, topath, mode='w', parents=True, exist_ok=True):

    to_path = pathlib.Path(topath)

    to_path.parent.mkdir(parents=parents, exist_ok=exist_ok)

    if not to_path.exists():
        to_path.touch()

    if to_path.exists() and to_path.is_file():
        with open(topath, mode) as outfile:
            outfile.write(data)
    else:
        print("Warning: unable to write to %s" % to_path)


def read_from(frompath, bytes=False):

    from_path = pathlib.Path(frompath)

    if bytes:
        return from_path.read_bytes()
    else:
        return from_path.read_text()


def install_mirrorlist(args):

    # Check if gateway is a mirror

    if ':' in args.localmirror:
        host, port = args.localmirror.split(':')
    else:
        host, port = args.localmirror, 80

    exists = False

    # Prioritize local mirror if we can contact one

    with contextlib.closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        sock.settimeout(2)
        if sock.connect_ex((host, int(port))) == 0:
            write_to('Server = http://%s:%s/$repo/os/$arch\n' % (host, port), '/etc/pacman.d/mirrorlist')

    # Make sure dependencies exist

    run.quit_on_error(['pacman', '-Syy'], 'pacman failed updating mirrors?')

    run.quit_on_error(['pacman', '-S', 'archlinux-keyring', '--noconfirm', '--overwrite', "'*'"], 'pacman failed installing keyring?')

    if args.distro == 'parabola':

        run.quit_on_error(['pacman', '-S', 'parabola-keyring', '--noconfirm', '--overwrite', "'*'"], 'pacman failed installing keyring?')

    # Takes way too long.
    #run.warn_on_error(['pacman-key', '--refresh-keys'], 'failed refreshing keys?')
    #run.warn_on_error(['pacman-key', '--updatedb'], 'failed updating keys?')

    # TODO: So this just failed, tries to grab an older pacman-contrib version for some reason, likely because older archlinux iso.
    # TODO: Perhaps use reflector as a backup and second backup after that maybe just don't rank the mirrors?
    # TODO: external urls will also fail on offline install so maybe its for the better to just make them all failable

    run.quit_on_error(['pacman', '-S', 'pacman-contrib', '--noconfirm', '--overwrite', "'*'"], 'pacman failed installing contrib?')

    # Run rankmirrors

    url = False

    if args.distro == 'archlinux':
        url = 'https://archlinux.org/mirrorlist/?country=NO&country=SE&protocol=https&ip_version=4'

    if args.distro == 'parabola':
        url = 'https://parabola.nu/mirrorlist/?country=SE&protocol=https&ip_version=4'

    if not url:
        print('Error: mirrorlist url empty. Quitting.')
        sys.exit()

    # TODO: for air-gapped installs (with only a local package server) these next commands should not be run, or silently fail.

    try:
        mirrors = urllib.request.urlopen(url).read().decode('utf8').replace('#Server', 'Server')

        # temp parabola fix..

        if args.distro == 'parabola':
            mirrors = 'Server = https://ftp.acc.umu.se/mirror/parabola.nu/$repo/os/$arch\nServer = https://ftpmirror1.infania.net/parabola/$repo/os/$arch'

        # TODO: yo this could probably be a stdin instead

        write_to(mirrors, 'mirrorlist')
        ranked_mirrors = subprocess.run(['rankmirrors', '-n', '6', 'mirrorlist'], stdout=subprocess.PIPE).stdout.decode('utf8')
        if args.distro == 'parabola':
            ranked_mirrors = ranked_mirrors + '\n' + ('Server = https://redirector.parabola.nu/$repo/os/$arch\n'*5)
        write_to(ranked_mirrors, '/etc/pacman.d/mirrorlist', mode='w')
    except:
        print('Warning: unable to rank mirrors. Assuming offline / air-gapped install.')
        args.offline = True


def initialize_pacman(args):

    # I think pacstrapped system inherits /etc/pacman.conf, right?
    config = read_from('./configs/pacman.conf')

    # Uncomment parabola specific repositories
    if args.distro == 'parabola':
        config = config.replace('#', '')

    write_to(config, '/etc/pacman.conf')

    run.quit_on_error(['pacman', '-Syy'], 'pacman failed updating mirrors?')

    run.quit_on_error(['pacman', '-S', args.distro + '-keyring', '--noconfirm'], 'pacman failed installing keyring?')


def partition(args):

    run.ignore_error(['umount', '-l', args.disk])

    run.quit_on_error(['parted', '-s', args.disk, 'mklabel', 'gpt'], 'parted (mklabel) failed')

    # UEFI

    if args.uefi:

        run.quit_on_error(['parted', '-s', '-a', 'optimal', args.disk, 'mkpart', 'primary', '251MiB', '100%'], 'parted (primary) failed')

        run.quit_on_error(['parted', '-s', '-a', 'optimal', args.disk, 'mkpart', 'primary', '1MiB', '251MiB'], 'parted (uefi) failed')

        run.quit_on_error(['mkfs.fat', '-F', '32', get_partition(args, 2)])

    else:

        run.quit_on_error(['parted', '-s', '-a', 'optimal', args.disk, 'mkpart', 'primary', '0%', '100%'], 'parted (primary) failed')

        # Bugfix for gpt:

        run.quit_on_error(['parted', '-s', args.disk, 'set', '1', 'boot', 'off'], 'parted (boot off) failed')

        run.quit_on_error(['parted', '-s', args.disk, 'set', '1', 'bios_grub', 'on'], 'parted (bios_grub on) failed')

    if args.install == 1 or args.install == 3:

        # Encrypted

        run.quit_on_error(['cryptsetup', '--cipher', 'aes-xts-plain64', '--key-size', '512', '--hash', 'whirlpool', 'luksFormat', '--iter-time', '2000', '--type', 'luks1', '--use-random', get_partition(args), '-d', '-', ], 'cryptsetup format failed', args.encpass)

        run.quit_on_error(['cryptsetup', 'open', get_partition(args), 'cryptroot', '--key-file=-'], 'cryptsetup open failed', args.encpass)

        run.quit_on_error(['mkfs.ext4', '-O', 'fast_commit', '-F', '/dev/mapper/cryptroot'])

        run.quit_on_error(['mount', '/dev/mapper/cryptroot', '/mnt'])

    else:

        # Unencrypted

        run.quit_on_error(['mkfs.ext4', '-F', get_partition(args)])

        run.quit_on_error(['mount', get_partition(args), '/mnt'])

    if args.uefi:

        # Make /mnt/efi on host and mount the ESP. This turns into /efi in chroot.
        os.makedirs('/mnt/efi', exist_ok=True)
        run.quit_on_error(['mount', get_partition(args, 2), '/mnt/efi'])


def pacstrap(args, config):

    if args.install == 1:
        packages = json.loads(config['packages']['desktop'])

    if args.install == 2 or args.install == 3:
        packages = json.loads(config['packages']['minimal'])

    if args.install == 4:
        packages = json.loads(config['packages']['vm'])

    if not packages:
        print('Error: no packages?')
        sys.exit()

    if args.distro == 'parabola':
        packages += ['linux-libre' + config['system']['kernel'], 'linux-libre-firmware', 'linux-libre' + config['system']['kernel'] + '-headers']
    else:
        packages += ['linux' + config['system']['kernel'], 'linux-firmware', 'linux' + config['system']['kernel'] + '-headers']

    if args.uefi:

        packages += ['efibootmgr']


    if args.gpu == 'NVIDIA':

        packages += ['nvidia' + config['system']['kernel'], 'lib32-nvidia-utils', 'nvidia-settings']

    if args.gpu == 'Intel':

        packages += ['mesa', 'lib32-mesa']

    # Microcode

    with open('/proc/cpuinfo', 'r') as cpuinfo:
        for line in cpuinfo.readlines():
            if 'Intel' in line:
                packages += ['intel-ucode']
                break
            if 'AMD' in line:
                packages += ['amd-ucode']
                break

    # Filter not found packages

    filtered = []

    for package in packages:
        if run.return_bool(['pacman', '-Ss', '^'+package+'$']):
            filtered.append(package)

    run.quit_on_error(['pacstrap', '/mnt'] + filtered, 'pacstrap failed', '', 0)


def genfstab():

    run.quit_on_error(['genfstab', '-U', '-p', '/mnt', '>>', '/mnt/etc/fstab'])


def prepare(args):

    copy_to('./configs', '/mnt/configs')

    copy_to('./install.py', '/mnt/install.py')

    run.quit_on_error(['chmod', '+x', '/mnt/install.py'])

    copy_to('./install.ini', '/mnt/install.ini')

    copy_to('/etc/pacman.d/mirrorlist', '/mnt/etc/pacman.d/mirrorlist')

    copy_to('/etc/pacman.conf', '/mnt/etc/pacman.conf')

    # Grub started acting up

    os.makedirs('/mnt/hostrun', exist_ok=True)

    run.quit_on_error(['mount', '--bind', '/run', '/mnt/hostrun'], 'unable to bind /run')

    # Actually chroot

    outargs = [
        'arch-chroot', '/mnt', './install.py',
        '-i', str(args.install),
        '-d', args.disk,
        '-n', args.hostname,
        '-u', args.username,
        '-ep', args.encpass,
        '-rp', args.rootpass,
        '-up', args.userpass,
        '--gpu', args.gpu,
        '--boot', args.boot,
        '--chroot',
    ]

    if args.offline:
        outargs.append('--offline')

    if args.uefi:
        outargs.append('--uefi')

    run.quit_on_error(outargs)


def chroot(args):

    # GPU

    os.makedirs('/etc/pacman.d/hooks/', exist_ok=True)

    if args.gpu == 'NVIDIA':

        copy_to('/configs/hooks/nvidia.hook', '/etc/pacman.d/hooks/nvidia.hook')

    # Paccache

    copy_to('/configs/hooks/paccache.hook', '/etc/pacman.d/hooks/paccache.hook')

    # Locale

    write_to('en_US.UTF-8 UTF-8\nLANG=en_US.UTF-8\n', '/etc/locale.gen')

    write_to('LANG=en_US.UTF-8', '/etc/locale.conf')

    run.quit_on_error(['locale-gen'])

    # Boot key

    if args.install == 1 or args.install == 3:

        run.quit_on_error(['dd', 'bs=512', 'count=4', 'if=/dev/urandom', 'of=/crypto_keyfile.bin'])

        run.quit_on_error(['cryptsetup', 'luksAddKey', get_partition(args), '/crypto_keyfile.bin', '--key-file=-'], 'luksAddKey failed', args.encpass)

    # mkinitcpio

    modules = []

    if args.gpu == 'NVIDIA':
        modules += ['nvidia', 'nvidia_modeset', 'nvidia_uvm', 'nvidia_drm']

    if args.uefi:
        modules += ['vmd']

    for line in fileinput.input('/etc/mkinitcpio.conf', inplace=True):
        if line.startswith('HOOKS='):
            print('HOOKS=(base systemd udev autodetect microcode kms keyboard keymap consolefont modconf block sd-encrypt filesystems fsck)', end='')
            continue

        if args.install == 1 or args.install == 3:
            if line.startswith('FILES='):
                print('FILES=(/crypto_keyfile.bin)', end='')
                continue

        if args.gpu == 'NVIDIA':
            if line.startswith('MODULES='):
                print('MODULES=(%s)' % (' '.join(modules)), end='')
                #print('MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)', end='')
                continue

        print(line, end='')

    if args.distro == 'parabola':
        run.quit_on_error(['mkinitcpio', '-p', 'linux-libre' + config['system']['kernel']])
    else:
        run.quit_on_error(['mkinitcpio', '-p', 'linux' + config['system']['kernel']])

    # Grub started acting up

    os.makedirs('/boot/grub', exist_ok=True)

    # Grub

    if args.install == 1 or args.install == 3:
        write_to('GRUB_ENABLE_CRYPTODISK=y\n', '/etc/default/grub', mode='a')

    for line in fileinput.input('/etc/default/grub', inplace=True):
        if line.startswith('GRUB_TIMEOUT='):
            print('GRUB_TIMEOUT=2\n', end='')
            continue
        if line.startswith('GRUB_PRELOAD_MODULES='):
            if args.install == 1 or args.install == 3:
                print('GRUB_PRELOAD_MODULES="part_gpt part_msdos cryptodisk luks"', end='')
            else:
                print('GRUB_PRELOAD_MODULES="part_gpt part_msdos"', end='')
            continue
        if line.startswith('GRUB_CMDLINE_LINUX_DEFAULT='):
            if args.install == 1 or args.install == 3:
                if args.gpu == 'NVIDIA':
                    print('GRUB_CMDLINE_LINUX_DEFAULT="init_on_free=1 nowatchdog rd.luks.name=%s=cryptroot root=/dev/mapper/cryptroot rd.luks.key=/crypto_keyfile.bin nvidia-drm.modeset=1"\n' % get_uuid(get_partition(args)), end='')
                else:
                    print('GRUB_CMDLINE_LINUX_DEFAULT="init_on_free=1 nowatchdog rd.luks.name=%s=cryptroot root=/dev/mapper/cryptroot rd.luks.key=/crypto_keyfile.bin"\n' % get_uuid(get_partition(args)), end='')
            continue
        print(line, end='')

    run.quit_on_error(['grub-mkconfig', '-o', '/boot/grub/grub.cfg'])

    if args.install == 1 or args.install == 3:

        run.quit_on_error(['chmod', '000', '/crypto_keyfile.bin'])

    # SystemD

    run.ignore_error(['systemctl', 'disable', 'dhcpcd.service'])

    run.ignore_error(['systemctl', 'enable', 'ntpd.service'])

    run.ignore_error(['systemctl', 'enable', 'NetworkManager.service'])

    run.ignore_error(['systemctl', 'enable', 'linux-modules-cleanup.service'])

    if args.install == 1:

        run.ignore_error(['systemctl', 'enable', 'lightdm.service'])

        run.ignore_error(['systemctl', 'enable', 'tlp.service'])

        run.ignore_error(['systemctl', 'enable', 'tlp-sleep.service'])

    # System configuration

    write_to('KEYMAP=no\n', '/etc/vconsole.conf')

    write_to(args.hostname + '\n', '/etc/hostname')

    run.ignore_error(['useradd', '-m', '-s', '/bin/bash', '%s' % args.username])

    run.quit_on_error(['usermod', '--password', crypt(args.userpass), args.username])

    run.quit_on_error(['usermod', '--password', crypt(args.rootpass), 'root'])

    write_to('%s	ALL=(ALL) ALL\n' % args.username, '/etc/sudoers', 'a')

    run.quit_on_error(['ln', '-f', '-s', '/usr/share/zoneinfo/Europe/Oslo', '/etc/localtime'])

    run.quit_on_error(['hwclock', '--systohc'])

    # Swapfile

    run.quit_on_error(['dd', 'if=/dev/zero', 'of=/swapfile', 'bs=1M', 'count=' + str(config['system']['swap'])])

    run.quit_on_error(['chmod', '600', '/swapfile'])

    run.quit_on_error(['mkswap', '/swapfile'])

    run.quit_on_error(['swapon', '/swapfile'])

    #with open('/etc/crypttab', 'w+') as locale:
    #locale.write('cryptroot UUID=%s /crypto_keyfile.bin luks' % get_uuid(get_partition(args)))
    #write_to('/dev/mapper/cryptroot / ext4 defaults 0 1\n', '/etc/fstab)

    if args.uefi:
        write_to('UUID=%s /efi none defaults 0 0\n' % get_uuid(get_partition(args, 2)), '/etc/fstab', mode='a')

        # Had a problem with a system that wasn't able to mount EFI:
        copy_to('/configs/modules/vfat.conf', '/etc/modules-load.d/vfat.conf')

        # Had a problem with a system that wasn't able to mount the freaking first partition..
        copy_to('/configs/modules/vmd.conf', '/etc/modules-load.d/vmd.conf')

    write_to('/swapfile none swap defaults 0 0\n', '/etc/fstab', mode='a')
    write_to('none /home/%s/.thumbnails tmpfs rw,noexec,nosuid,size=10%%,uid=1000,gid=1000,mode=0755 0 0\n' % args.username, '/etc/fstab', mode='a')
    write_to('none /home/%s/.cache/thumbnails tmpfs rw,noexec,nosuid,size=10%%,uid=1000,gid=1000,mode=0755 0 0\n' % args.username, '/etc/fstab', mode='a')

    # Login Config

    copy_to('/configs/faillock.conf', '/etc/security/faillock.conf')

    # Fix annoying sudo delay on CTRL-C

    for line in fileinput.input('/etc/pam.d/system-auth', inplace=True):
        if line.startswith('auth') and ('pam_faillock.so' in line or 'pam_unix.so' in line):
            print(line.strip() + ' nodelay')
            continue

        print(line, end='')

    # User configs

    # Minimal - shared across every install

    write_to('export MAKEFLAGS=\'-j 8\'\n', '/etc/profile')

    copy_to('/configs/bashrc', '/home/%s/.bashrc' % args.username)

    if not args.nonpersonal:

        copy_to('/configs/gitconfig', '/home/%s/.gitconfig' % args.username)

    copy_to('/configs/gitignore_global', '/home/%s/.gitignore_global' % args.username)

    copy_to('/configs/bash_profile', '/home/%s/.bash_profile' % args.username)

    copy_to('/configs/micro', '/home/%s/.config/micro' % args.username)

    copy_to('/configs/mimeapps.list', '/home/%s/.config/mimeapps.list' % args.username)

    os.makedirs('/home/%s/.ssh/' % args.username, exist_ok=True) # Making sure this exists

    copy_to('/configs/bin', '/home/%s/.bin' % args.username)

    copy_to('/configs/blacklist.conf', '/etc/modprobe.d/blacklist.conf')

    copy_to('/configs/login.defs', '/etc/login.defs')

    copy_to('/configs/systemd/00-journal-size.conf', '/etc/systemd/journald.conf.d/00-journal-size.conf')

    copy_to('/configs/network.mount', '/usr/share/gvfs/mounts/network.mount')
    copy_to('/configs/network.mount', '/usr/share/gvfs/network.mount')

    # Desktop only

    if args.install == 1:

        copy_to('/configs/xfce4', '/home/%s/.config/xfce4/xfconf/xfce-perchannel-xml' % args.username)

        write_to('file:///home/%s/Downloads\n' % args.username, '/home/%s/.config/gtk-3.0/bookmarks' % args.username)

        write_to('[Settings]\ngtk-recent-files-max-age=0\ngtk-recent-files-limit=0\n', '/home/%s/.config/gtk-3.0/settings.ini' % args.username)

        write_to('gtk-recent-files-max-age=0\n', '/home/%s/.gtkrc-2.0' % args.username, mode='a')

        run.warn_on_error(['su', args.username, '-c', 'gsettings set org.gnome.desktop.privacy remember-recent-files false', '-s', '/bin/bash', args.username])
        run.warn_on_error(['su', args.username, '-c', 'gsettings set org.gnome.desktop.privacy remember-app-usage false', '-s', '/bin/bash', args.username])
        run.warn_on_error(['su', args.username, '-c', 'gsettings set org.gnome.desktop.privacy recent-files-max-age 0', '-s', '/bin/bash', args.username])

        copy_to('/configs/panel/whiskermenu-2.rc', '/home/%s/.config/xfce4/panel/whiskermenu-2.rc' % args.username)

        copy_to('/configs/terminalrc', '/home/%s/.config/xfce4/terminal/terminalrc' % args.username)
        copy_to('/configs/accels.scm', '/home/%s/.config/xfce4/terminal/accels.scm' % args.username)

        if not args.nonpersonal:

            copy_to('/configs/pypirc', '/home/%s/.pypirc' % args.username)

        copy_to('/configs/npmrc', '/home/%s/.npmrc' % args.username)

        copy_to('/etc/xdg/xfce4/xinitrc', '/home/%s/.xinitrc' % args.username)

        copy_to('/configs/network-manager.desktop', '/home/%s/.config/autostart/network-manager.desktop' % args.username)

        copy_to('/configs/kwalletrc', '/home/%s/.config/kwalletrc' % args.username)

        copy_to('/configs/toggle-media', '/usr/bin/toggle-media')
        run.quit_on_error(['chmod', '755', '/usr/bin/toggle-media'])

        copy_to('/configs/vscodium', '/home/%s/.config/VSCodium/User/settings.json' % args.username)

        copy_to('/configs/20-keyboard.conf', '/etc/X11/xorg.conf.d/20-keyboard.conf')

        copy_to('/configs/pulse/daemon.conf', '/etc/pulse/daemon.conf')

        copy_to('/configs/pulse/default.pa', '/etc/pulse/default.pa')

        copy_to('configs/helpers.rc', '/etc/xdg/xfce4/helpers.rc')

        copy_to('configs/mount.rc', '/etc/xdg/xfce4/mount.rc')

        copy_to('/configs/viewnior.conf', '/home/%s/.config/viewnior/viewnior.conf' % args.username)

        copy_to('/configs/librewolf.overrides.cfg', '/home/%s/.librewolf/librewolf.overrides.cfg' % args.username)

        copy_to('/configs/mpv.conf', '/home/%s/.config/mpv/mpv.conf' % args.username)

        copy_to('/configs/mullvad', '/home/%s/.config/Mullvad VPN/gui_settings.json' % args.username)

        copy_to('/configs/uca.xml', '/home/%s/.config/Thunar/uca.xml' % args.username)

        # Mousepad

        dconf = ''

        with open('/configs/mousepad/mousepad.dconf', 'r') as mp:
            dconf = mp.read()

        # I doubt this will work but we'll try
        # run.warn_on_error(['su', args.username, '-c', 'dconf', 'load', '/org/xfce/mousepad/'], stdin=dconf)
        # It does not work.

        copy_to('/configs/mousepad/dracula.xml', '/home/%s/.local/share/gtksourceview-4/styles/dracula.xml' % args.username)

        if not args.offline:

            try:
                write_to(urllib.request.urlopen('https://raw.githubusercontent.com/dracula/wallpaper/master/xubuntu.png').read(), '/usr/share/backgrounds/xfce/dracula.png', 'wb')

                # qbittorrent

                write_to(urllib.request.urlopen('https://github.com/dracula/qbittorrent/raw/master/qbittorrent.qbtheme').read(), '/home/%s/.config/qBittorrent/qbittorrent.qbtheme' % args.username)

                copy_to('/configs/qBittorrent', '/home/%s/.config/qBittorrent' % args.username)

            except:
                print("Warning: unable to download extra rice from the net")

        copy_to('/configs/lightdm-gtk-greeter.conf', '/etc/lightdm/lightdm-gtk-greeter.conf')

        #run.quit_on_error(['pip3', 'install', '--exists-action=w', 'liberate'])

    # Cleanup

    shutil.rmtree('/configs')

    # Make sure permissions are right

    os.makedirs('/home/%s/.cache' % args.username, exist_ok=True)

    run.quit_on_error(['chown', '-R', '%s:%s' % (args.username, args.username), '/home/%s/' % args.username])

    write_to(str(datetime.date.today()) + '\n', '/installed')

    # Install from AUR

    if not args.offline:

        write_to('nobody ALL=(ALL) NOPASSWD: ALL\n', '/etc/sudoers', 'a')

        run.quit_on_error(['usermod', '--expiredate', str(datetime.date.today().replace(year=datetime.date.today().year+1)), 'nobody'])

        # Recv keys

        keys = json.loads(config['keys']['all'])

        for key in keys:
            run.ignore_error(['su', args.username, '-c', 'gpg --recv-keys %s' % key])
            run.ignore_error(['su', 'nobody', '-c', 'gpg --recv-keys %s' % key]) # TODO: didn't work

        os.makedirs('/build', exist_ok=True)

        run.quit_on_error(['chown', 'nobody:root', '-R', '/build'])

        os.chdir('/build')

        aur_packages = json.loads(config['aur']['all'])

        if args.install == 1:

            aur_packages += json.loads(config['aur']['desktop'])

        for package in aur_packages:

            run.quit_on_error(['su', 'nobody', '-c', 'git clone https://aur.archlinux.org/%s.git' % package, '-s', '/bin/bash', 'nobody'])

            os.chdir('/build/' + package)

            run.warn_on_error(['su', 'nobody', '-c', 'makepkg -i --noconfirm PKGBUILD', '-s', '/bin/bash', 'nobody'])

            os.chdir('/build')

        os.chdir('/')

        shutil.rmtree('/build')

    for line in fileinput.input('/etc/sudoers', inplace=True):
        if line.startswith('nobody ALL='):
            continue
        print(line, end='')

    # Actually install grub, moved last because aur/grub-improved-luks2-git is needed

    bootdisk = args.disk
    if args.boot:
        bootdisk = args.boot

    if args.uefi:
        # '--modules=%s' % (' '.join(json.loads(config['grub']['modules']))),
        run.quit_on_error(['grub-install', '--target=x86_64-efi', '--efi-directory=/efi', '--bootloader-id=GRUB', '--removable', '--recheck', bootdisk])

    else:

        run.quit_on_error(['grub-install', '--target=i386-pc', '--recheck', bootdisk])

    run.quit_on_error(['chmod', '-R', 'g-rwx,o-rwx', '/boot'])

    run.quit_on_error(['chmod', 'og-rwx', '/etc/default/grub'])

    run.quit_on_error(['chmod', '-R', 'og-rwx', '/etc/grub.d'])


if __name__ == '__main__':

    signal.signal(signal.SIGINT, signal_handler)

    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--disk', dest='disk', type=str, help='Disk to install to', default='')
    parser.add_argument('-n', '--hostname', dest='hostname', type=str, help='Computer hostname', default='')
    parser.add_argument('-u', '--username', dest='username', type=str, help='Username to use', default='')
    parser.add_argument('-i', '--install', dest='install', type=int, help='Type of install', default=0)
    parser.add_argument('-l', '--list', dest='list', help='List types of install', action='store_true')
    parser.add_argument('-ep', '--encpass', dest='encpass', type=str, help='Encryption password')
    parser.add_argument('-rp', '--rootpass', dest='rootpass', type=str, help='Root password')
    parser.add_argument('-up', '--userpass', dest='userpass', type=str, help='User password')
    parser.add_argument('--noconfirm', dest='noconfirm', help='Do not ask for confirmation', action='store_true')
    parser.add_argument('--distro', dest='distro', type=str, help='Distro being installed', default=determine_distro())
    parser.add_argument('--dev', dest='dev', help='Run in development mode', action='store_true')
    parser.add_argument('--chroot', dest='chroot', help='Chroot flag', action='store_true')
    parser.add_argument('--uefi', dest='uefi', help='UEFI flag', action='store_true')
    parser.add_argument('--secureboot', dest='secureboot', help='Secureboot flag, only applicable for UEFI')
    parser.add_argument('--no-uefi', dest='nouefi', help='No UEFI override', action='store_true')
    parser.add_argument('--nonpersonal', dest='nonpersonal', help='No personal configs override', action='store_true')
    parser.add_argument('--localmirror', dest='localmirror', help='Local mirror server override', default=determine_gateway())
    parser.add_argument('--gpu', dest='gpu', type=str, help='GPU override (NVIDIA, AMD, Intel, Hybrid, None)', default=determine_gpu())
    parser.add_argument('--offline', dest='offline', help='Offline / air-gapped install', action='store_true')
    parser.add_argument('-b', '--boot', dest='boot', type=str, help='Optionally install grub to another drive', default='')
    args = parser.parse_args()

    config = configparser.ConfigParser()
    config.read_file(open('install.ini'))

    if args.list:
        print(args.distro)
        list_installs()
        sys.exit()

    if args.chroot:
        chroot(args)
        sys.exit()

    print('Welcome! Let\'s get you set up. Hit CTRL-C to quit.')

    get_install(args)

    get_disk(args)

    get_hostname(args)

    get_username(args)

    confirm_options(args)

    get_passwords(args)

    if os.path.exists('/sys/firmware/efi'):

        args.uefi = True

    if args.nouefi:

        args.uefi = False

    # Actually install

    if not args.dev:

        print('Updating mirrors...')

        install_mirrorlist(args)

        initialize_pacman(args)

        print('Partitioning...')

        partition(args)

        print('Pacstrapping...')

        pacstrap(args, config)

        print('Chrooting...')

        genfstab() # TODO: this seems to do nothing so maybe just remove it

        prepare(args)  # chroots

        os.remove('/mnt/install.ini')

        os.remove('/mnt/install.py')

        run.quit_on_error(['umount', '-R', '/mnt/'])

        run.quit_on_error(['systemctl', 'poweroff'])
